package com.frezneel.SpringBlog.users.services;

import com.frezneel.SpringBlog.users.dtos.requests.RegisterUserRequest;
import com.frezneel.SpringBlog.users.entites.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {
    List<User> getAllUsers();

    Optional<User> getUserById(Long id);

    Optional<User> getUserByEmail(String email);

    void save(RegisterUserRequest registerUserRequest);


}