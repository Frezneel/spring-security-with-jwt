package com.frezneel.SpringBlog.users.repository;

import com.frezneel.SpringBlog.users.entites.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM users WHERE users.email = :email", nativeQuery = true)
    Optional<User> findByEmail(String email);
}
