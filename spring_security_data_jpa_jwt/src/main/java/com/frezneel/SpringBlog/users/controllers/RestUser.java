package com.frezneel.SpringBlog.users.controllers;

import com.frezneel.SpringBlog.users.dtos.requests.LoginUserRequest;
import com.frezneel.SpringBlog.users.dtos.requests.RegisterUserRequest;
import com.frezneel.SpringBlog.users.dtos.responses.UserResponse;
import com.frezneel.SpringBlog.users.entites.User;
import com.frezneel.SpringBlog.users.services.JwtService;
import com.frezneel.SpringBlog.users.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestUser {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @GetMapping("/users")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<?> getAllUsers(){
        Map<String, Object> result = new HashMap<>();
        try {
            List<User> users = userService.getAllUsers();
            List<UserResponse> userResponse = users.stream()
                    .map(user -> {
                        UserResponse response = new UserResponse();
                        response.setName(user.getName());
                        response.setEmail(user.getEmail());
                        return response;
                    }).collect(Collectors.toList());
            result.put("status", "200");
            result.put("message", "Data Successfully Acquiring");
            result.put("data", userResponse);
            result.put("dataNoFilter", users);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            result.put("status", "500");
            result.put("message", "Failed Acquiring Data");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<?> saveUsers(@RequestBody RegisterUserRequest registerUserRequest){
        Map<String, Object> result = new HashMap<>();
        try {
            userService.save(registerUserRequest);
            UserResponse userResponse = new UserResponse();
            userResponse.setEmail(registerUserRequest.getEmail());
            userResponse.setName(registerUserRequest.getName());
            result.put("status", "200");
            result.put("message", "Data Successfully Saved");
            result.put("data", userResponse);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            result.put("status", "500");
            result.put("message", "Failed Saving Data");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users/login")
    public ResponseEntity<?> loginUsers(@RequestBody LoginUserRequest loginUserRequest){
        Map<String, Object> result = new HashMap<>();
        try {
            Authentication authenticationRequest =
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                            loginUserRequest.getEmail(), loginUserRequest.getPassword()));
            if (authenticationRequest.isAuthenticated()) {
                Optional<User> user = userService.getUserByEmail(loginUserRequest.getEmail());
                UserResponse userResponse = new UserResponse(user.get().getEmail(), user.get().getName());
                result.put("status", "200");
                result.put("info", "SUCCESS");
                result.put("message", "Login sucessfully");
                result.put("data", userResponse);
                result.put("auth", authenticationRequest);
                result.put("token", jwtService.generateToken(loginUserRequest.getEmail()));
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("status", "200");
                result.put("info", "FAILED");
                result.put("message", "Account not authenticated");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            result.put("status", "200");
            result.put("info", "FAILED");
            result.put("message", "Invalid user/password");
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

}
