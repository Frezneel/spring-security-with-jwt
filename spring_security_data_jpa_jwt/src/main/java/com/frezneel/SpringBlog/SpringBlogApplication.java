package com.frezneel.SpringBlog;

import com.frezneel.SpringBlog.configurations.BCryptConfig;
import com.frezneel.SpringBlog.users.entites.User;
import com.frezneel.SpringBlog.users.repository.UserRepo;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Locale;

@SpringBootApplication
public class SpringBlogApplication {

	@Autowired
 	private UserRepo userRepo;
	@Autowired
	private BCryptConfig bCryptConfig;

	public static void main(String[] args) {
		SpringApplication.run(SpringBlogApplication.class, args);
	}

}
