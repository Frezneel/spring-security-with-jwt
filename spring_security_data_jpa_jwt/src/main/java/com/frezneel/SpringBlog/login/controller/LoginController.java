package com.frezneel.SpringBlog.login.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class LoginController {

    @PostMapping("login-user")
    public String index(){
        return "login/index";
    }

}
